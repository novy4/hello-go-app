FROM golang:1.11.0-alpine3.8

WORKDIR /app

ENV SRC_DIR=/go/src/gitlab.com/novy4/hello-go-app/

ADD . $SRC_DIR

RUN apk add --no-cache wget curl git build-base
RUN go get github.com/gorilla/mux
RUN ls -la $SRC_DIR 
RUN cd $SRC_DIR; go build -o hello; cp hello /app/

ENTRYPOINT [ "./hello" ]
